import 'package:flutter/material.dart';

import 'package:reg_project/LoginPage.dart';


class profile extends StatefulWidget{
  @override
  _profile createState() =>  _profile();
}

class _profile extends State<profile> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, colors: [
            Colors.grey,
            Colors.yellow,
          ]),
        ),
        child: Column(
          children: <Widget>[
            Expanded(child: Container(
              width: 400,
              decoration: BoxDecoration(
                  color: const Color.fromRGBO(255, 250, 250, 0.5),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(80),
                    topRight: Radius.circular(80),
                    bottomLeft: Radius.circular(80),
                    bottomRight: Radius.circular(80),
                  )
              ),
              child: buildProfileWidget(),
            ),
            )
          ],
        ),
      ),
    );
  }


  Widget buildProfileWidget() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(5),
        child: Column(
            children: [
              const SizedBox(height: 10),
              SizedBox(
                width: 120, height: 120,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: const Image(image: AssetImage('assets/profile.jpg'))),
              ),
              Text("Karntima MaginBUU",style: TextStyle(fontSize: 20),),
              SizedBox(height:20),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(height:20),
                  Text(
                      "ประวัตินิสิต",
                      style: new TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          foreground: Paint()..shader = LinearGradient(
                            colors: <Color>[
                              Colors.grey,
                              Colors.yellow,
                              Colors.black45,
                              //add more color here.
                            ],
                          ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 100.0))
                      )
                  ),
                ],
              ),
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(height: 10),
                    Text("รหัสประจำตัว: 63160280",style: TextStyle(fontSize: 15),),
                    Text("ชื่อ: นางสาวกานต์ติมา ฝาเรือนดี",style: TextStyle(fontSize: 15),),
                    Text("ชื่ออังกฤษ: MISS KARNTIMA FARUEANDEE",style: TextStyle(fontSize: 15),),
                    Text("คณะ: คณะวิทยาการสารสนเทศ ",style: TextStyle(fontSize: 15),),
                  ]
              ),
              SizedBox(height:15),
              Container(
                width: 335,
                height: 25,
                color: Color.fromRGBO(255, 222, 89, 5),
                child: Row(
                    children: [
                      SizedBox(width:10),
                      Text("ข้อมูลด้านการศึกษา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,),),
                    ]
                ),
              ),
              SizedBox(height:10),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("คณะ:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width: 125),
                    Text("คณะวิทยาการสารสนเทศ"),
                  ]
              ),SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("วิทยาเขต:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width: 100),
                    Text("บางแสน"),
                  ]
              ),SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("หลักสูตร:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width: 105),
                    Text("2115020(คอมพิวเตอร์)"),
                  ]
              ),SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("ระดับการศึกษา",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width:73),
                    Text("ปริญญาตรี"),
                  ]
              ),SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("ชื่อปริญญา:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width:90),
                    Text("วิทยาศาสตรบัณฑิต วท.บ."),
                  ]
              ),SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("ปีการศึกษาที่เข้า:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width: 65),
                    Text("2563/1 วันที่ 10/6/2563"),
                  ]
              ),SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("วิธีรับเข้า:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width: 107),
                    Text("Admission กลาง"),
                  ]
              ),SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("วุฒิก่อนเข้ารับการศึกษา:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width:20),
                    Text("ม.6"),
                  ]
              ),
              SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("จบการศึกษาจาก:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width:60),
                    Text("สตรีเศรษฐบุตรบำเพ็ญ"),
                  ]
              ),
              SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("อ.ที่ปรึกษา:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width:95),
                    Text("อาจารย์"),
                  ]
              ),
              SizedBox(height:15),
              Container(
                width: 340,
                height: 25,
                color: Color.fromARGB(255, 214, 220, 57),
                child: Row(
                    children: [
                      SizedBox(width:10),
                      Text("ผลการศึกษา",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,),),
                    ]
                ),
              ), SizedBox(height:10),

              Row(
                  children: [
                    SizedBox(width:38),
                    Text("หน่วยกิตคำนวณ:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width: 60),
                    Text("91"),
                  ]
              ),SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("หน่วยกิตที่ผ่าน:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width: 70),
                    Text("91"),
                  ]
              ),SizedBox(height:5),
              Row(
                  children: [
                    SizedBox(width:38),
                    Text("คะแนนเฉลี่ยสะสม:",style: TextStyle(fontSize: 15 ,)),
                    SizedBox(width: 50),
                    Text("3.36"),
                  ]
              ),
              SizedBox(height:50),
              Row(
                children: [
                  buildButton(context),
                ],
              ),
            ]
        ),
      ),
    );
  }
}

Widget buildButton(BuildContext context) {
  return Container(
    height: 50,
    margin: EdgeInsets.symmetric(horizontal: 50),
    decoration: BoxDecoration(
      color: Colors.red,
      borderRadius: BorderRadius.circular(10),
    ),
    child: ElevatedButton(
      child: Text("Logout",style: TextStyle(
          color: Colors.white,
          fontSize: 15,
          fontWeight: FontWeight.bold
      )),
      onPressed: (){
        Navigator.push(
          context, MaterialPageRoute(builder: (context) => LoginPage()),
        );
      },
    ),
  );
}
