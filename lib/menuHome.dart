import 'package:reg_project/Home.dart';
import 'package:flutter/material.dart';
import 'package:reg_project/crouseClass.dart';
import 'package:reg_project/studiedResult.dart';
import 'package:reg_project/suggest.dart';
import 'package:reg_project/weblog.dart';
import 'package:reg_project/approve.dart';
import 'package:reg_project/enroll.dart';
import 'package:reg_project/gradCheck.dart';


class menuHome extends StatefulWidget{
  @override
  _menuHome createState() =>  _menuHome();
}

class _menuHome extends State<menuHome> {

  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Column(
        children: <Widget>[
          buildMenu1(context),
          buildMenu2(context),
          buildMenu3(context),
          buildMenu4(context),
          buildMenu5(context),
          buildMenu6(context),
          buildMenu7(context),
        ],
      ),
    );
  }
}

Widget buildMenu1 (BuildContext context){
  return Padding(
      padding: EdgeInsets.all(5),
    child: ElevatedButton(
      child: Image(image: AssetImage('assets/ตารางเรียนนิสิต.png'),width: 320,),
      onPressed: (){
        Navigator.push(
          context, MaterialPageRoute(builder: (context) => crouseClass()),
        );
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromRGBO(126, 217, 87, 5),
      ),
    ),
  );
}
Widget buildMenu2 (BuildContext context){
  return Padding(
    padding: EdgeInsets.all(5),
    child: ElevatedButton(
      child:Image(image: AssetImage('assets/ลงทะเบียนเรียน.png'),width: 320,),
      onPressed: (){
        Navigator.push(
          context, MaterialPageRoute(builder: (context) => enroll()),
        );
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromRGBO(92, 225, 230, 5),
      ),
    ),
  );
}
Widget buildMenu3 (BuildContext context){
  return Padding(
    padding: EdgeInsets.all(5),
    child: ElevatedButton(
      child: Image(image: AssetImage('assets/ผลการอนุมัติเพิ่มลด.png'),width: 320,),
      onPressed: (){
        Navigator.push(
          context, MaterialPageRoute(builder: (context) => appeove()),
        );
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromRGBO(255, 222, 89, 5),
      ),
    ),
  );
}
Widget buildMenu4 (BuildContext context){
  return Padding(
    padding: EdgeInsets.all(5),
    child: ElevatedButton(
      child: Image(image: AssetImage('assets/ผลการศึกษา.png'),width: 320,),
      onPressed: (){
        Navigator.push(
          context, MaterialPageRoute(builder: (context) => studiedResult()),
        );
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromRGBO(248, 180, 152, 5),
      ),
    ),
  );
}
Widget buildMenu5 (BuildContext context){
  return Padding(
    padding: EdgeInsets.all(5),
    child: ElevatedButton(
      child: Image(image: AssetImage('assets/ตรวจสอบจบ.png'),width: 320,),
      onPressed: (){
        Navigator.push(
          context, MaterialPageRoute(builder: (context) => gradCheck()),
        );
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromRGBO(203, 108, 230, 5),
      ),
    ),
  );
}
Widget buildMenu6 (BuildContext context){
  return Padding(
    padding: EdgeInsets.all(5),
    child: ElevatedButton(
      child: Image(image: AssetImage('assets/เสนอความคิดเห็น.png'),width: 320,),
      onPressed: (){
        Navigator.push(
          context, MaterialPageRoute(builder: (context) => suggest()),
        );
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromRGBO(255, 189, 89, 5),
      ),
    ),
  );
}
Widget buildMenu7 (BuildContext context){
  return Padding(
    padding: EdgeInsets.all(5),
    child: ElevatedButton(
      child: Image(image: AssetImage('assets/ประวัติการเข้าใช้งานระบบ.png'),width: 320,),
      onPressed: (){
        Navigator.push(
          context, MaterialPageRoute(builder: (context) => weblog()),
        );
      },
      style: ElevatedButton.styleFrom(
        primary: Color.fromRGBO(166, 166, 166, 5),
      ),
    ),
  );
}