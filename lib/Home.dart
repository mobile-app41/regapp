import 'package:flutter/material.dart';
import 'package:reg_project/menuHome.dart';
import 'package:reg_project/profile.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const HomeScreen());
  }
}
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final List<Widget> _screens = [
    // Content for Home tab
    Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, colors: [
            Colors.grey,
            Colors.yellow,
          ])
      ),
      alignment: Alignment.center,
      child: buildImg(),
    ),
    Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, colors: [
            Colors.grey,
            Colors.yellow,
          ])
      ),
      alignment: Alignment.center,
      child: menuHome(),
    ),
    Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, colors: [
            Colors.grey,
            Colors.yellow,
          ])
      ),
      alignment: Alignment.center,
      child: profile(),
    )
  ];
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Burapha University"),
        leadingWidth: 10,
        backgroundColor: Colors.yellow,
      ),
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _selectedIndex,
          backgroundColor: Colors.grey,
          unselectedItemColor: Colors.yellow,
          selectedItemColor: Colors.white,
          // called when one tab is selected
          onTap: (int index) {
            setState(() {
              _selectedIndex = index;
            });
          },
          // bottom tab items
          items: const [
            BottomNavigationBarItem(
                icon: Icon(Icons.feed), label: 'Feed'),
            BottomNavigationBarItem(
                icon: Icon(Icons.home), label: 'Home'),
            BottomNavigationBarItem(
                icon: Icon(Icons.person), label: 'Profile')
          ]),
      body: Row(
        children: [
          if (MediaQuery.of(context).size.width >= 1500)
            NavigationRail(
              onDestinationSelected: (int index) {
                setState(() {
                  _selectedIndex = index;
                });
              },
              selectedIndex: _selectedIndex,
              destinations: const [
                NavigationRailDestination(
                    icon: Icon(Icons.feed), label: Text('Feed')),
                NavigationRailDestination(
                    icon: Icon(Icons.home), label: Text('Home')),
                NavigationRailDestination(
                    icon: Icon(Icons.person), label: Text('Profile')),
              ],
              labelType: NavigationRailLabelType.all,
              selectedLabelTextStyle: const TextStyle(
                color: Colors.yellow,
              ),
              unselectedLabelTextStyle: const TextStyle(),
            ),
          Expanded(child: _screens[_selectedIndex])
        ],
      ),
    );
  }
}


Widget buildImg(){
  return SingleChildScrollView(
    scrollDirection: Axis.vertical,
    child: Column(
      children:<Widget> [
        Image(image: AssetImage('assets/S1.jpg'),width: 370,),
        Image(image: AssetImage('assets/S2.jpg'),width: 370,)
      ],
    ),
  );
}
