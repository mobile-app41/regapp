import 'package:flutter/material.dart';

class crouseClass extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        title: const Text("ตารางเรียนนิสิต"),
        backgroundColor: Colors.yellow.shade500,
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, colors: [
            Colors.grey,
            Colors.yellow,
          ]),
        ),
        child: buildCrouse(),
      ),
    );
  }
}


Widget buildCrouse(){
  return Padding(
    padding: const EdgeInsets.all(5),
    child: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height:20),
          Text(
              "ตารางเรียน/สอบของรายวิชาที่ลงทะเบียนไว้แล้ว",
              style: new TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  foreground: Paint()..shader = LinearGradient(
                    colors: <Color>[
                      Colors.grey,
                      Colors.yellow,
                      Colors.black45,
                      //add more color here.
                    ],
                  ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 100.0))
              )
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                Text("ชื่อ	กานต์ติมา ฝาเรือนดี",style: TextStyle(fontSize: 15),),
                Text("สถานภาพ	กำลังศึกษา",style: TextStyle(fontSize: 15),),
                Text("หลักสูตร	วท.บ. (วิทยาการคอมพิวเตอร์)",style: TextStyle(fontSize: 15),),
              ]
          ),
          SizedBox(height: 20,),
          Column(
            children: [
              Image(image: AssetImage('assets/ตารางสอน.png'),width: 345,)
            ],
          ),
          SizedBox(height: 20,),
          Column(
            children: [
              Image(image: AssetImage('assets/ตารางสอบ.png'),width: 345,)
            ],
          )
        ],
      ),
    ),
  );
}
