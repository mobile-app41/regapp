import 'package:flutter/material.dart';

class enroll extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        title: const Text("ลงทะเบียนเรียน"),
        backgroundColor: Colors.yellow.shade500,
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, colors: [
            Colors.grey,
            Colors.yellow,
          ]),
        ),
        child: buildEnroll(),
      ),
    );
  }
}


Widget buildEnroll(){
  return Padding(
    padding: const EdgeInsets.all(5),
    child: Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height:20),
          Text(
              "ผลลงทะเบียน",
              style: new TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  foreground: Paint()..shader = LinearGradient(
                    colors: <Color>[
                      Colors.grey,
                      Colors.yellow,
                      Colors.black45,
                      //add more color here.
                    ],
                  ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 100.0))
              )
          ),
          Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 10),
                Text("ชื่อ	กานต์ติมา ฝาเรือนดี",style: TextStyle(fontSize: 15),),
                Text("สถานภาพ	กำลังศึกษา",style: TextStyle(fontSize: 15),),
                Text("หลักสูตร	วท.บ. (วิทยาการคอมพิวเตอร์)",style: TextStyle(fontSize: 15),),
              ]
          ),
          SizedBox(height: 20,),
          Column(
            children: [
              Text("ปีการศึกษา 2565/2 ",style: TextStyle(fontSize: 15),),
              Image(image: AssetImage('assets/ผลลงทะเบียน.png'),width: 345,)
            ],
          ),
        ],
      ),
    ),
  );
}
