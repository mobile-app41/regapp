import 'package:flutter/material.dart';
import 'package:reg_project/Home.dart';


class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, colors: [
            Colors.grey,
            Colors.yellow,
          ]),
        ),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 250,),
            buildHeader(),
            Expanded(child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(60),
                    topRight: Radius.circular(60),
                  )
              ),
              child: buildWrapper(context),
            ))
          ],
        ),
      ),
    );
  }
}
Widget buildHeader() {
  return Padding(
    padding: EdgeInsets.zero,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Center (
          child: Text("มหาวิทยาลัยบูรพา", style: TextStyle(color: Colors.yellow, fontSize: 50),),
        ),
        SizedBox(height: 5,),
        Center(
          child: Text("BURAPHA UNIVERSITY", style: TextStyle(color: Colors.yellow, fontSize: 20),),
        )
      ],
    ),
  );
}

Widget buildWrapper(BuildContext context){
  return Padding(
    padding: EdgeInsets.all(30),
    child: Column(
      children: <Widget>[
        SizedBox(height: 40,),
        Container(
          decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(10)
          ),
          child: buildInputField(),
        ),
        SizedBox(height: 40,),
        Text(
          "Forgot Password?",
          style: TextStyle(color: Colors.grey),
        ),
        SizedBox(height: 40,),
        buildButton(context),
      ],
    ),
  );
}

Widget buildInputField() {
  return Column(
    children: <Widget>[
      Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: Colors.grey)
            )
        ),
        child: TextField(
          decoration: InputDecoration(
              hintText: "Enter your Student ID ",
              hintStyle: TextStyle(color: Colors.white),
              border: InputBorder.none
          ),
        ),
      ),
      Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: Colors.white)
            )
        ),
        child: TextField(
          decoration: InputDecoration(
              hintText: "Enter your password",
              hintStyle: TextStyle(color: Colors.white),
              border: InputBorder.none
          ),
        ),
      ),
    ],
  );
}

Widget buildButton(BuildContext context) {
  return Container(
    height: 50,
    margin: EdgeInsets.symmetric(horizontal: 50),
    decoration: BoxDecoration(
      color: Colors.green,
      borderRadius: BorderRadius.circular(10),
    ),
    child: ElevatedButton(
      child: Text("Login",style: TextStyle(
          color: Colors.white,
          fontSize: 15,
          fontWeight: FontWeight.bold
      )),
      onPressed: (){
        Navigator.push(
          context, MaterialPageRoute(builder: (context) =>Home()),
        );
      },
    ),
  );
}

