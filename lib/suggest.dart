import 'package:flutter/material.dart';

class suggest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        title: const Text("เสนอความคิดเห็น"),
        backgroundColor: Colors.yellow.shade500,
      ),
      body: Container(
        width: double.infinity,
        decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, colors: [
            Colors.grey,
            Colors.yellow,
          ]),
        ),
        child: buildSggest(),
      ),
    );
  }


Widget buildSggest() {
  return Padding(
    padding: EdgeInsets.all(5),
    child: Column(
      children: [
        const SizedBox(height: 10,),
        Image(image: AssetImage('assets/burapha.jpg'),width: 370,),
        SizedBox(height: 10,),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(height:20),
            Text(
                "เชิญร่วมแสดงความคิดเห็น",
                style: new TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    foreground: Paint()..shader = LinearGradient(
                      colors: <Color>[
                        Colors.grey,
                        Colors.yellow,
                        Colors.black45,
                        //add more color here.
                      ],
                    ).createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 100.0))
                )
            ),
            const SizedBox(
              width: 345,
              height:300 ,
              child:TextField(
                style: TextStyle(fontSize: 15.0,  color: Colors.black),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                ),
              ),),
            Column(
              children: [
                TextButton(
                    style: ButtonStyle(
                      foregroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                      overlayColor: MaterialStateProperty.resolveWith<Color?>(
                            (Set<MaterialState> states) {
                          if (states.contains(MaterialState.hovered))
                            return Colors.blue.withOpacity(0.04);
                          if (states.contains(MaterialState.focused) ||
                              states.contains(MaterialState.pressed))
                            return Colors.blue.withOpacity(0.12);
                          return null; // Defer to the widget's default.
                        },
                      ),
                    ),
                    onPressed: () { },
                    child: Text('ส่งความคิดเห็น')
              ),
              ],
            )
          ],
        ),
      ],
    ),
  );
}
}
